HOST = localhost
PORT = 8080

SRC = server
INTF = init_func


main:
	python3 -m aiohttp.web -H $(HOST) -P $(PORT) $(SRC):$(INTF)
	
session:
	python3 -m aiohttp.web -H $(HOST) -P $(PORT) session:make_app
